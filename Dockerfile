FROM python:3
MAINTAINER Stefano Tranquillini <stefano@chino.io>
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
RUN pip install uwsgi
ADD requirements.txt /code/
RUN pip install -r requirements.txt
ADD . /code/
RUN chmod +x startup.sh
CMD ["./startup.sh"]