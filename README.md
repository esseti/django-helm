# hello-world

hello world app to test integration with CI/CD and Kube.

- install docker
- install minkube
- install helm
- run docker
- `minikube start`
- `eval $(minikube docker-env)`
- `helm init`
- `helm install --set production=false/true --name hwdj` (see what prints out)
=======
# django helm

Basic django project that uses helm (within minkube) and reads env variables from configMap.

# ELK

uses docker compose and an ad-hoc image, we use this as development tool to understnad the integration of django+ELK stack. In production ELK should be deployed in a more consistent fashion.