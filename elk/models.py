from django.db.models import Model, fields


# Create your models here.

class SomeObject(Model):
    creation = fields.DateTimeField(auto_now_add=True)
    update = fields.DateTimeField(auto_now=True)
    content = fields.CharField(max_length=256)
