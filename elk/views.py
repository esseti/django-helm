import django.views.generic as views
from django.http import Http404
from django.urls import reverse_lazy
from django.views.generic import DeleteView

from elk.models import SomeObject


# Create your views here.

class CreateSomeObject(views.CreateView):
    model = SomeObject
    fields = ['content']
    template_name = 'elk/create.html'

    def get_success_url(self):
        return reverse_lazy('elk:details', args=(self.object.id,))


class ViewTheObjects(views.ListView):
    model = SomeObject
    template_name = 'elk/list.html'


class SomeObjectInfo(views.DetailView):
    model = SomeObject
    template_name = 'elk/item.html'


class DeleteSomeObject(DeleteView):
    model = SomeObject
    success_url = reverse_lazy('elk:list')
    template_name = 'elk/item.html'
    extra_context = {'confirm_delete': True}


def not_found(_):
    return Http404()


def internal_error(_):
    raise Exception("This error should be logged.")
