from django.urls import path, include

from elk import views

urlpatterns = [
    path('', views.ViewTheObjects.as_view(), name='list'),
    path('view/<slug:pk>/', views.SomeObjectInfo.as_view(), name='details'),
    path('new/', views.CreateSomeObject.as_view(), name='create'),
    path('view/<slug:pk>/delete/', views.DeleteSomeObject.as_view(), name='delete'),
    path('404', views.not_found, name='404'),
    path('500', views.internal_error, name='500'),
]
