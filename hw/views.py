from django.conf import settings
from django.http import HttpResponse
from django.shortcuts import render

import os
import environ
import random
from time import sleep
from django.http import Http404


# Create your views here.


def index(request):
    value = random.uniform(0, 3)
    if value < 1:
        raise Http404
    if value > 2.5:
        print(request['gimme a 500'])
    sleep(random.uniform(1, 5))
    return HttpResponse("Hello, world. %s %s %s %s" % (
        value, settings.TEST_VALUE, os.environ.get('TEST_VALUE', 'not found'),
        os.environ.get('TEST_VALUE2', 'not found')))
